function calc_percents(raw_numbers) {
  const remainders_dict = {};
  const remainders_list = [];
  let total_remainder = 100;
  const summ = raw_numbers.reduce(function (a, b) {
    return a + b;
  });

  for (let x of raw_numbers) {
    const perc = (x * 100) / summ;
    const inter_perc = ~~((x * 100) / summ);
    total_remainder = total_remainder - inter_perc;
    remainders_dict[perc - inter_perc] = inter_perc;
    remainders_list.push(perc - inter_perc);
  }

  const slr = [...remainders_list].sort(function (a, b) {
    return a - b;
  });

  slr.reverse();

  counter = 0;

  while (counter < total_remainder) {
    const y = slr[counter];
    remainders_dict[y] = remainders_dict[y] + 1;
    counter = counter + 1;
  }

  const output_percentages = [];

  for (let z of remainders_list) {
    output_percentages.push(remainders_dict[z]);
  }

  return output_percentages;
}

const raw_numbers = [12, 46, 789, 3, 111];

for (let i = 1, len = 100_000; i < len; i++) {
  const percents = calc_percents(raw_numbers.concat([i]));
  // console.log(percents);
}
