def calc_percents(raw_numbers):
    remainders_dict = {}
    remainders_list = []
    total_remainder = 100
    summ = sum(raw_numbers)

    for x in raw_numbers:
        perc = x * 100 / summ
        inter_perc = x * 100 // summ
        total_remainder = total_remainder - inter_perc
        remainders_dict[perc - inter_perc] = inter_perc
        remainders_list.append(perc - inter_perc)

    slr = sorted(remainders_list)
    slr.reverse()
    counter = 0

    while counter < total_remainder:
        y = slr[counter]
        remainders_dict[y] = remainders_dict[y] + 1
        counter = counter + 1

    output_percentages = []

    for z in remainders_list:
        output_percentages.append(remainders_dict[z])

    return output_percentages


if __name__ == "__main__":
    raw_numbers = [12, 46, 789, 3, 111]
    for i in range(1, 100000):
        percents = calc_percents(raw_numbers + [i])
        # print(percents)
