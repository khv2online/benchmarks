def calc_percents(raw_numbers)
  summ = raw_numbers.reduce(&:+)
  raw_numbers.map! { |n| (n * 100 / summ) }
  diff = 100 - raw_numbers.reduce(&:+)
  rounded_percentages = raw_numbers
    .sort_by { |x| x.floor - x}
    .map
    .with_index { |e, index| index < diff ? e.floor + 1 : e.floor }
end

(1..100000).each do |i|
  raw_numbers = [12, 46, 789, 3, 111]
  rounded_percentages = calc_percents raw_numbers + [i] 
  # puts rounded_percentages
end
